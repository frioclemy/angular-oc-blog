import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  posts = [
    {
      title: 'Premier post',
      content: 'Lorem ipsum ...',
      loveIts: 2
    },
    {
      title: 'Second post',
      content: 'Lorem ipsum ...',
      loveIts: 0
    },
    {
      title: 'Troisième post',
      content: 'Lorem ipsum ...',
      loveIts: -1
    }
  ];

  constructor() {  }
}

